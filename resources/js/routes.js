import Vue from 'vue';
import VueRouter from  'vue-router';
import {call} from './services/API'

import Hello from './views/Hello'
import Splash from './views/Splash'

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'splash',
      component: Splash
    },
    {
      path: '/hello',
      name: 'hello',
      component: Hello,
    },
    {
      path: '/try',
      name: 'try',
      component: function () {
        console.log(call());
      },
    },
  ],
});

export default router;